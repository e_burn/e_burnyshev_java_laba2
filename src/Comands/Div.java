package Comands;

import Main.Context;

import java.util.Vector;

public class Div implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs) {
        double b = context.popFromStack();
        double a = context.popFromStack();
        context.pushToStack(a/b);
    }
}
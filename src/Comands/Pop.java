package Comands;

import Main.Context;

import java.util.Vector;


public class Pop implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs ) {
        context.popFromStack();
    }
}
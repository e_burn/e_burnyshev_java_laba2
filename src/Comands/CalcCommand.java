package Comands;

import Main.Context;

import java.util.Vector;

public interface CalcCommand {

    public void execute(Context context , Vector<String> myArgs );

}
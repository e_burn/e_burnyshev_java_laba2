package Comands;

import Main.Context;

import java.util.Vector;

public class Summ implements CalcCommand {

    public void execute(Context context, Vector<String> myArgs ) {
        double a = context.popFromStack();
        double b = context.popFromStack();
        context.pushToStack(a+b);
    }

}
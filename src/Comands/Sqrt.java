package Comands;

import Main.Context;

import java.util.Vector;


public class Sqrt implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs ) {
        double a = context.popFromStack();
        context.pushToStack( Math.sqrt(a) );
    }
}
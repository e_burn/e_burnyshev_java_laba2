package Comands;

import Main.Context;

import java.util.Vector;


public class Push implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs ) {
        //System.out.println( "PUSH myArgs0: " +myArgs.get(0) );
        double arg;
        try {
            arg = Double.parseDouble(myArgs.get(0));
        } catch (NumberFormatException e) {
            arg = context.getConstant(myArgs.get(0));
        }
        context.pushToStack(arg);
    }
}
package Comands;

import Main.Context;

import java.util.Vector;

public class Define implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs) {
        if (myArgs.size() >= 2){
            context.setConstant( myArgs.get(0) , Double.parseDouble(myArgs.get(1)) );
        }
        else
        {
            System.out.println("You may use DEFINE as \"DEFINE paramName paramValue\"!");
        }
    }
}
package Comands;

import Main.Context;

import java.util.Vector;


public class Print implements CalcCommand{
    public void execute(Context context, Vector<String> myArgs) {
        double a = context.peekAtStack();
        System.out.println("Result: " +a);
    }
}
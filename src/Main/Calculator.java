package Main;

import Comands.CalcCommand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

public class Calculator {

    private static Context context = new Context();

    public static void main(String args[]) throws IOException {
        //Context context = null;
        String cmd[];
        String commandStr;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            System.out.println("Enter command:");
            commandStr = in.readLine();

            if( commandStr.equalsIgnoreCase("q")){
                in.close();
                System.out.println(commandStr);
                System.exit(0);
            }
            cmd = commandStr.split(" ");
            Vector<String> myArgs = new Vector<String>();
            if(cmd.length >= 2) {
                myArgs.add(cmd[1]);
            }
            if(cmd.length >= 3) {
                myArgs.add(cmd[2]);
            }
            try {
                CalcCommand command = CmdFactory.getCommand( cmd[0] );
                command.execute( context , myArgs );
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

}
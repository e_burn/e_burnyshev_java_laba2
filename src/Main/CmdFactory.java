package Main;

import java.io.FileInputStream;
import Comands.CalcCommand;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.TreeMap;

public class CmdFactory {

    private static TreeMap<String, String> binds = null;
    private static Properties properties = null;
    private static TreeMap<String, Class<CalcCommand>> classes = null;

    static {
        binds = new TreeMap<String, String>();
        classes = new TreeMap<String, Class<CalcCommand>>();
        InputStream conformancesAsStream = null;
        try {
            conformancesAsStream = new FileInputStream("src/config.properties");
            properties = new Properties();
            properties.load(conformancesAsStream);
        } catch (IOException e) {
            System.out.println("IOException occurred in config.properties " + e.getLocalizedMessage());
        } finally {
            try {
                conformancesAsStream.close();
            } catch (IOException e) {
                System.out.println("IOException: " + e.getLocalizedMessage());
            }
        }
    }

    public static CalcCommand getCommand(String cmdName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        CalcCommand command = null;
        Class<?> cmdClass = null;
        //System.out.println(cmdName);
        String commandUp = cmdName.toUpperCase();
        //System.out.println(commandUp);

        String key = properties.getProperty(commandUp);
        if (null == key) {
            throw new NoSuchElementException("Invalid command request: " + commandUp);
        }
        if (!classes.containsKey(key)) {
            cmdClass = Class.forName(key);
            classes.put ( cmdName, (Class<CalcCommand>)cmdClass );
        } else {
            cmdClass = classes.get(cmdName);
        }
        command = (CalcCommand) cmdClass.newInstance();

        return command;
    }
}